package com.Roman.Book.Management.Application;

public class BookInformation extends Book {
    private String authorFirstName;
    private String authorLastName;

    public BookInformation () {}

    public BookInformation(String authorFirstName, String authorLastName) {
        this.authorFirstName = authorFirstName;
        this.authorLastName = authorLastName;
    }

    public BookInformation(int bookId, int bookYear, String bookName, String bookDescription, String authorFirstName, String authorLastName) {
        super(bookId, bookYear, bookName, bookDescription);
        this.authorFirstName = authorFirstName;
        this.authorLastName = authorLastName;
    }


    public String getAuthorFirstName() {
        return authorFirstName;
    }

    public void setAuthorFirstName(String authorFirstName) {
        this.authorFirstName = authorFirstName;
    }

    public String getAuthorLastName() {
        return authorLastName;
    }

    public void setAuthorLastName(String authorLastName) {
        this.authorLastName = authorLastName;
    }

}