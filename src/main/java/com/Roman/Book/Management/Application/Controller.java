package com.Roman.Book.Management.Application;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
public class Controller {

    @Autowired
    private JdbcTemplate jdbctemplate;

    @Autowired
    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    public Controller() {
    }

    // Küsib põhivaadet nr 1 - autorite järgi.
    @GetMapping("/authors")
    public List<Author> getAuthors() {
        return getAllAuthors();
    }


    // Kõik autorid listina. Autorite vaade.
    public List<Author> getAllAuthors() {

        List<Author> authors = jdbctemplate.query("SELECT * FROM author",
                (row, count) -> {
                    int id = row.getInt("ID");
                    String authorFirstName = row.getString("authorFirstName");
                    String authorLastName = row.getString("authorLastName");

                    Author author = new Author(id, authorFirstName, authorLastName);
                    return author;

                }
        );
        return authors;
    }


    // Iga autori raamatud. Raamatute vaade.
    @GetMapping("/authorbooks")
    public List<Book> getAuthorAllBooks(String keyword) {

        List<Integer> authorIds = getAuthorsIds(keyword);
        MapSqlParameterSource parameters = new MapSqlParameterSource();
        parameters.addValue("authorID", authorIds);

        List<Book> books = namedParameterJdbcTemplate.query(
                "SELECT bookAuthor.bookID, book.bookName, book.year, book.id, book.description "
                        + "FROM book "
                        + "JOIN bookAuthor ON book.id = bookAuthor.bookID "
                        + "WHERE bookAuthor.authorID IN (:authorID) ",
                parameters,
                (row, count) -> {
                    int id = row.getInt("id");
                    int year = row.getInt("year");
                    String bookName = row.getString("bookName");
                    String bookDescription = row.getString("description");

                    Book book = new Book(id, year, bookName, bookDescription);
                    return book;
                }
        );

        List<Book> authorBooks = new ArrayList<>();

        for (Book b : books) {
            if (!authorBooks.contains(b.getBookId())) {
                authorBooks.add(b);
            }
        }

        return authorBooks;

    }


    // Autorite ID-d listina
    private List<Integer> getAuthorsIds(String keyword) {

        List<Integer> authorIdList = new ArrayList<>();
        List<Author> authors = getAllAuthors();

        for (Author a : authors) {
            if (a.getAuthorLastName().equalsIgnoreCase(keyword))
                authorIdList.add(a.getId());
        }
        return authorIdList;
    }


    // Raamatute ID-d listina keywordi järgi
    private List<Integer> getBooksIds(String keyword) {

        List<Integer> bookIdList = new ArrayList<>();
        List<Book> books = getAllBooks();

        for (Book b : books) {
            if (b.bookName.equalsIgnoreCase(keyword))
                bookIdList.add(b.getBookId());
        }
        return bookIdList;
    }


    // Küsib põhivaadet nr. 2 - aastate järgi.
    @GetMapping("/books")
    public Map<Integer, List<Book>> getBooksByYear() {
        return getAllBooksByYear();
    }


    // Kõik raamatud listina
    private List<Book> getAllBooks() {

        List<Book> books = jdbctemplate.query("SELECT * FROM book",
                (row, count) -> {
                    int id = row.getInt("ID");
                    int year = row.getInt("year");
                    String bookName = row.getString("bookName");
                    String bookDescription = row.getString("description");

                    Book book = new Book(id, year, bookName, bookDescription);
                    return book;

                }
        );
        return books;
    }


    // Võtab raamatute listi ja loob iga väljalaske aasta kohta uue listi sel aastal välja antud raamatutega.
    private Map<Integer, List<Book>> getAllBooksByYear() {

        List<Book> books = getAllBooks();
        Map<Integer, List<Book>> years = new HashMap<>();

        for (Book b : books) {
            if (!years.containsKey(b.getBookYear())) {
                years.put(b.getBookYear(), new ArrayList<>());
                years.get(b.getBookYear()).add(b);
            } else {
                years.get(b.getBookYear()).add(b);
            }
        }
        return years;
    }



    // Konkreetse raamatu infopäring
    @GetMapping("/bookinformation")
    public List<BookInformation> getBookInformation(@RequestParam String keyword) {

        List<BookInformation> authorBook = jdbctemplate.query(
                "SELECT bookAuthor.bookID, bookAuthor.authorID, author.authorFirstName, author.authorLastName, book.bookName, book.year, book.id, book.description "
                        + "FROM book "
                        + "INNER JOIN bookAuthor ON bookAuthor.bookID = book.id and bookAuthor.authorID = author.id "
                        + "INNER JOIN author ON author.id = bookAuthor.authorID "
                        + " WHERE book.bookName LIKE ? OR book.description LIKE ?",
                new Object[] {"%" + keyword + "%", "%" + keyword + "%"},
                (row, count) -> {
                    int id = row.getInt("ID");
                    int year = row.getInt("year");
                    String bookName = row.getString("bookName");
                    String authorFirstName = row.getString("authorFirstName");
                    String authorLastName = row.getString("authorLastName");
                    String description = row.getString("description");

                    BookInformation book = new BookInformation(id, year, bookName, authorFirstName, authorLastName, description);
                    return book;


                }
        );
        return authorBook;

    }



    // Raamatu lisamine andmebaasi
    @PostMapping("/addbook")
    public void addBook(@RequestBody BookInformation book) {
        addBookToDatabase(book);
    }

    public void addBookToDatabase(BookInformation book) {
        String authorLastName = "";
        String authorFirstName = "";

        for (Author a : getAllAuthors()) {
            if (a.getAuthorLastName().equalsIgnoreCase(book.getAuthorLastName())) {
                if (!authorLastName.equalsIgnoreCase(a.getAuthorLastName())) {
                    authorLastName = a.getAuthorLastName();
                }
            }
        }
        for (Author a : getAllAuthors()) {
            if (a.getAuthorFirstName().equalsIgnoreCase(book.getAuthorFirstName())) {
                if (!authorFirstName.equalsIgnoreCase(a.getAuthorFirstName())) {
                    authorFirstName = a.getAuthorFirstName();
                }
            }
        }


        if (!getAllBooks().contains(book.getBookName())) {
            if (!authorLastName.equalsIgnoreCase(book.getAuthorLastName()) && !authorFirstName.equalsIgnoreCase(book.getAuthorFirstName())) {
                jdbctemplate.update("INSERT INTO book (year, bookName, description) VALUES (?, ?, ?)", book.getBookYear(), book.getBookName(), book.getBookDescription());
                jdbctemplate.update("INSERT INTO author (authorFirstName, authorLastName) VALUES (?, ?)", book.getAuthorFirstName(), book.getAuthorLastName());
                jdbctemplate.update("INSERT INTO bookAuthor (bookID, authorID) VALUES (?, ?)", getBooksIds(book.getBookName()).get(0), getAuthorsIds(book.getAuthorLastName()).get(0));

            } else {
                jdbctemplate.update("INSERT INTO book (year, bookName, description) VALUES (?, ?, ?)", book.getBookYear(), book.getBookName(), book.getBookDescription());
                jdbctemplate.update("INSERT INTO bookAuthor (bookID, authorID) VALUES (?, ?)", getBooksIds(book.getBookName()).get(0), getAuthorsIds(book.getAuthorLastName()).get(0));

            }
        }


    }


}
