package com.Roman.Book.Management.Application;

public class Book {
    public int bookId;
    public int bookYear;
    public String bookName;
    public String bookDescription;

    public Book () {}

    public Book(int bookId, int bookYear, String bookName, String bookDescription) {
        this.bookId = bookId;
        this.bookYear = bookYear;
        this.bookName = bookName;
        this.bookDescription = bookDescription;
    }

    public String getBookDescription() {
        return bookDescription;
    }

    public void setBookDescription(String bookDescription) {
        this.bookDescription = bookDescription;
    }

    public int getBookId() {
        return bookId;
    }

    public void setBookId(int bookId) {
        this.bookId = bookId;
    }

    public int getBookYear() {
        return bookYear;
    }

    public void setBookYear(int bookYear) {
        this.bookYear = bookYear;
    }

    public String getBookName() {
        return bookName;
    }

    public void setBookName(String bookName) {
        this.bookName = bookName;
    }

}