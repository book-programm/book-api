package com.Roman.Book.Management.Application;

public class Author {
    private int id;
    private String authorFirstName;
    private String authorLastName;

    public Author(int id, String authorFirstName, String authorLastName) {
        this.id = id;
        this.authorFirstName = authorFirstName;
        this.authorLastName = authorLastName;
    }


    public String getAuthorFirstName() {
        return authorFirstName;
    }

    public void setAuthorFirstName(String authorFirstName) {
        this.authorFirstName = authorFirstName;
    }

    public String getAuthorLastName() {
        return authorLastName;
    }

    public void setAuthorLastName(String authorLastName) {
        this.authorLastName = authorLastName;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }


}