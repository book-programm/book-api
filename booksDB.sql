-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.3.12-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win64
-- HeidiSQL Version:             9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for books
DROP DATABASE IF EXISTS `books`;
CREATE DATABASE IF NOT EXISTS `books` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `books`;

-- Dumping structure for table books.author
DROP TABLE IF EXISTS `author`;
CREATE TABLE IF NOT EXISTS `author` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `authorFirstName` text NOT NULL,
  `authorLastName` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- Dumping data for table books.author: ~5 rows (approximately)
/*!40000 ALTER TABLE `author` DISABLE KEYS */;
INSERT INTO `author` (`id`, `authorFirstName`, `authorLastName`) VALUES
	(1, 'J. K.', 'Rowling'),
	(2, 'Robert C.', 'Martin'),
	(3, 'Kami', 'Garcia'),
	(4, 'Margaret', 'Stohl'),
	(5, 'J. R. R.', 'Tolkien');
/*!40000 ALTER TABLE `author` ENABLE KEYS */;

-- Dumping structure for table books.book
DROP TABLE IF EXISTS `book`;
CREATE TABLE IF NOT EXISTS `book` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `year` int(11) NOT NULL,
  `bookName` text NOT NULL,
  `description` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- Dumping data for table books.book: ~5 rows (approximately)
/*!40000 ALTER TABLE `book` DISABLE KEYS */;
INSERT INTO `book` (`id`, `year`, `bookName`, `description`) VALUES
	(1, 1997, 'Harry Potter and the Philosopher\'s Stone', 'This is story about one little wizzard boy and his friends.'),
	(2, 2009, 'Clean Code', 'Book is written for programmers to write cleaner and readable code.'),
	(3, 2009, 'Beautiful Creatures', 'The story takes place in a small town in Gatlin, South Carolina with a boy named Ethan Wate.  The town isn’t one of those storybook towns you read about in fairytales, it’s is as Ethan describes it, isolating and suffocating because everyone knows everyone.  Ethan dreams of escaping after high school to the farthest college away he can get to.  Ethan’s mother was killed in a car accident and after that his father became a withdrawn, absente dad, holed away in his study.  Though Ethan does have the company of his housekeeper Amma, who had been there to raise his dad when he was growing up.  Amma has the gift of foresight, and she is in some ways the town considers her the modern day witch.  Ethan had repeatedly had a dream about this girl falling, and he tries to catch her but is always just slightly out of reach.  When he wakes up, he smells rosemary and lemons, and on his iPod he hears the song, “Sixteen Moons” a song he has never ever heard up until the dreams started.  Later that day at school Ethan meets a girl named Lena Duchannes, she’s the new girl, who is the granddaughter of the infamous Macon Ravenwood.  Macon is a reclusive, mysterious owner of the Ravenwood Manor, an grand old plantation house from the times of the civil war, maybe even before then.  At school, Ethan suddenly hears “Sixteen Moons” and follows the sound to a music room where he finds Lena playing the song on the cello. He recognizes her as the girl he has dreamt about saving and is shocked to smell rosemary and lemon.  He cautiously approaches Lena and tells her about his dream he’s been having about her.  She denies knowing about anything, when Ethan tries to point out the scratches on her hand coinciding with the dream.  Over the next week Ethan slowly gains Lena’s trust by supporting her when students start teasing and bullying her.  They end up getting close and making a real connection, even so close that they start to develop the ability to communicate telepathically, no joke.  Lena later tells Ethan that her whole family, including her have special powers, referring to them as caster, originated from their ability to cast spells.  Ethan eventually learns to accept and even appreciate this fact after a while, because it’s a part of Lena.  Ethan meets a girl who approaches him at the convenient shop and introduces herself as Lena’s cousin named Ridley, who takes Ethan to Ravenwood manor.  Ridley’s real motives to taking Ethan to the manor is to sneak in and gain access to the house.  Ridley becomes angry at her family and declares that Lena will soon be “claimed” on her sixteenth birthday.  Lena knows Ridley speaks the truth.  The worst part of it all is that she will not have any say over if she will be claimed by the dark or the light.  Ethan ends up losing consciousness but not before he hears Lena tell Ridley to leave her “boyfriend” alone, or else.  Ethan wakes up in Lena’s bedroom, and Lena explains to Ethan that she can’t be with him because she may turn dark and lose the ability to love anyone, or anything.  Ethan tells Lena he will love her anyways no matter what she becomes and that he will try to save her from her fate.  At first, Macon and Amma both disapprove of the young teenage romance growing between Ethan and Lena and it becomes apparent that they have some history and prior connection involving the magical happenings in the town. However, this changes when Lena is attacked by evil forces on Halloween. Lena’s family are trying to protect her without success but, after Lena telepathically calls to him, Ethan manages to free her from the enchantment. After this, Lena’s family recognize the strength of Ethan and Lena’s relationship.  Amma comes to accept Ethan’s relationship with Lena and invites her over to Thanksgiving dinner at their home.  While they are enjoying the dinner everything suddenly freezes apart from Amma and Lena.  Ethan watches helplessly, immobilized, as Amma tells Lena that her mother Serafine isn’t dead but is dark and responsible for the attacks on Lena.  Ethan eavesdrops on Amma and Macon, learning that he is an incubus and is dark as well.  Lena’s whole family had come for the claiming ceremony, including her mother.  Serafine explains to Lena that she can choose whether she goes light or dark.  However Lena discovers the consequence of the choice:  If she chooses dark then every family member that is light will die, but if she chooses light then everyone who is dark in her family will die, including Uncle Macon, the one who she loves more than anything in the world.  She also learns that, regardless of her choice, as a Caster, she will not be able to have a physical relationship with a mortal like Ethan anyway, and is upset and confused.  On the night of the claiming, Lena’s mother stabs Ethan but Lena is able to bring him back but unfortunately Macon Ravenwood dies in his place.  As the Claiming requires, Lena causes the moon to disappear and it seems that she will have to choose Light or Dark. However, as the novel ends it is revealed that the song “Sixteen Moons” on Ethan’s iPod has transformed into “Seventeen Moons,” implying that Lena may be able to continue blocking out the moon and deferring her choice.  What will Lena decide, and will it cost her the boy she has fallen in love with?'),
	(4, 2001, 'Fantastic Beasts and Where to Find Them', 'It\'s about the magical creatures in the Harry Potter universe.'),
	(5, 1954, 'The Lord of the Rings', 'Thousands of years before the events of the novel, the Dark Lord Sauron had forged the One Ring to rule the other Rings of Power and corrupt those who wore them: three for Elves, seven for Dwarves, and nine for Men. Sauron was defeated by an alliance of Elves and Men led by Gil-galad and Elendil, respectively. In the final battle, Isildur, son of Elendil, cut the One Ring from Sauron\'s finger, causing Sauron to lose his physical form. Isildur claimed the Ring as an heirloom for his line, but when he was later ambushed and killed by the Orcs, the Ring was lost in the River Anduin. Over two thousand years later, the Ring was found by one of the river-folk called Déagol. His friend Sméagol fell under the Ring\'s influence and strangled Déagol to acquire it. Sméagol was banished and hid under the Misty Mountains. The Ring gave him long life and changed him over hundreds of years into a twisted, corrupted creature called Gollum. Gollum lost the Ring, his "precious", and as told in The Hobbit, Bilbo Baggins found it. Meanwhile, Sauron assumed a new form and took back his old realm of Mordor. When Gollum set out in search of the Ring, he was captured and tortured by Sauron. Sauron learned from Gollum that "Baggins" of the Shire had taken the Ring. Gollum was set loose. Sauron, who needed the Ring to regain his full power, sent forth his powerful servants, the Nazgûl, to seize it.');
/*!40000 ALTER TABLE `book` ENABLE KEYS */;

-- Dumping structure for table books.bookauthor
DROP TABLE IF EXISTS `bookauthor`;
CREATE TABLE IF NOT EXISTS `bookauthor` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `bookID` int(11) NOT NULL,
  `authorID` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

-- Dumping data for table books.bookauthor: ~6 rows (approximately)
/*!40000 ALTER TABLE `bookauthor` DISABLE KEYS */;
INSERT INTO `bookauthor` (`id`, `bookID`, `authorID`) VALUES
	(1, 1, 1),
	(2, 2, 2),
	(3, 3, 3),
	(4, 3, 4),
	(5, 4, 1),
	(6, 5, 5);
/*!40000 ALTER TABLE `bookauthor` ENABLE KEYS */;

-- Dumping structure for table books.users
DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` text NOT NULL,
  `password` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Dumping data for table books.users: ~0 rows (approximately)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
